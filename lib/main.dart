import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

void main(){
  runApp(Myapp());
}

class Myapp extends StatefulWidget {
  @override
  _MyappState createState() => _MyappState();
}

class _MyappState extends State<Myapp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      title: "My Calculator",
      home: homepage(),
    );
  }
}

class homepage extends StatefulWidget {
  @override
  _homepageState createState() => _homepageState();
}

class _homepageState extends State<homepage> {

  int firstnum,secnum;
  String txttodisplay="";
  String res;
  String operator;
  void btnclick(String value){

    if(value=="CE"){
      txttodisplay="";
      firstnum=0;
      secnum=0;
      res="";
    }
    else if(value=="+" || value=="-" || value=="x" || value=="/"){
      firstnum=int.parse(txttodisplay);
      res="";
      operator=value;
    }

    else if(value=="="){
      secnum=int.parse(txttodisplay);
      if(operator=="+"){
        res=(firstnum+secnum).toString();
      }
      if(operator=="x"){
        res=(firstnum*secnum).toString();
      }
      if(operator=="/"){
        res=(firstnum~/secnum).toString();
      }
      if(operator=="-"){
        res=(firstnum-secnum).toString();
      }
    }
    else{
      res=int.parse(txttodisplay+value).toString();
    }

    setState(() {
      txttodisplay=res;
    });
  }

  @override
  Widget custombutton(String btnvalue){
    return Expanded(
      child: OutlineButton(
        padding: EdgeInsets.all(25.0),
      onPressed: () => btnclick(btnvalue),
        child: Text(
          "$btnvalue",
          style: TextStyle(
            fontSize: 25.0,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "My Calculator"
        ),
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Expanded(
                child: Container(
                  padding: EdgeInsets.all(10.0),
                alignment: Alignment.bottomRight,
                  child: Text(
                      "$txttodisplay",
                    style: TextStyle(
                      fontSize: 30.0,
                      fontWeight: FontWeight.w600,

                    ),
                  ),
            ),
            ),
            Row(
              children: <Widget>[
                custombutton("9"),
                custombutton("8"),
                custombutton("7"),
                custombutton("+"),
              ],
            ),
            Row(
              children: <Widget>[
                custombutton("6"),
                custombutton("5"),
                custombutton("4"),
                custombutton("-"),
              ],
            ),
            Row(
              children: <Widget>[
                custombutton("3"),
                custombutton("2"),
                custombutton("1"),
                custombutton("x"),
              ],
            ),
            Row(
              children: <Widget>[
                custombutton("CE"),
                custombutton("="),
                custombutton("0"),
                custombutton("/"),
              ],
            ),
          ],
        ),
      ),
    );
  }
}



